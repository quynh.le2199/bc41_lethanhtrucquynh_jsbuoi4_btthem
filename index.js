/**
 * Bài tập 1
 * Input: nhập ngày, tháng, năm
 *
 * Các bước thực hiện:
 * 1) Ngày, tháng, năm của ngày tiếp theo:
 *    + Khai báo và gán biến date, month ,year theo giá trị của HTML element
 *    + Khai báo biến ngayHomQua, thangTruoc, namTruoc
 *    + Dùng cấu trúc if-else:
 *        Nếu ngày nhập vào bằng 1 thì:
 *           Nếu tháng nhập vào bằng 5 hoặc 7 hoặc 10 hoặc 12 thì ngayHomQua = 30; thangTruoc = month - 1; namTruoc = year;
 *           Nếu tháng nhập vào bằng 2 hoặc 4 hoặc 6 hoặc 8 hoặc 9 hoặc 11 thì ngayHomQua = 31; thangTruoc = month - 1; namTruoc = year;
 *           Nếu tháng nhập vào bằng 1 thì ngayHomQua = 31; thangTruoc = 12; namTruoc = year - 1;
 *           Trường hợp còn lại: 
 *              Nếu năm nhuận thì ngayHomQua = 29; thangTruoc = month - 1; namTruoc = year;
 *              Nếu năm không nhuận thì ngayHomQua = 28; thangTruoc = month - 1; namTruoc = year;
 *        Trường hợp còn lại: ngayHomQua = date - 1; thangTruoc = month; namTruoc = year;
 *    + In kết quả ra giao diện web
 *  
 * 2) Ngày tháng năm của ngày trước đó:
 *    + Khai báo và gán biến date, month ,year theo giá trị của HTML element
 *    + Khai báo biến ngayMai, thangSau, namSau
 *    + Dùng cấu trúc if-else:
 *        Nếu tháng nhập vào bằng 1 hoặc 3 hoặc 5 hoặc 7 hoặc 8 hoặc 10 thì:
 *           Nếu ngày nhập vào bằng 31 thì ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *           Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *        Nếu tháng nhập vào bằng 4 hoặc 6 hoặc 9 hoặc 11 thì:
 *           Nếu ngày nhập vào bằng nhỏ hơn hoặc bằng 30 thì:
 *              Nếu ngày bằng 30 thì: ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *              Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *           Trường hợp còn lại: in dòng chữ "Dữ liệu không hợp lệ"
 *        Nếu tháng nhập vào bằng 2 thì:
 *           Nếu năm nhập vào là năm nhuận thì:
 *              Nếu ngày bằng 29 thì: ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *              Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *           Nếu năm nhập vào không là năm nhuận thì:  
 *              Nếu ngày bằng nhỏ hơn hoặc bằng 28 thì:
 *                 Nếu ngày bằng 28 thì ngayMai = 1; thangSau = month + 1; namSau = year và in kết quả ra giao diện web
 *                 Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 *              Trường hợp còn lại: in dòng chữ "Dữ liệu không hợp lệ"
 *        Trường hợp còn lại: 
 *              Nếu ngày bằng 31 thì ngayMai = 1; thangSau = 1; namSau = year + 1 và in kết quả ra giao diện web
 *              Trường hợp còn lại: ngayMai = date + 1; thangSau = month; namSau = year và in kết quả ra giao diện web
 * 
 * Output: ngày, tháng, năm của ngày tiếp theo; ngày tháng năm của ngày trước đó
 */
 
function baiTap1a() {
  var date = document.getElementById("date").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var ngayHomQua, thangTruoc, namTruoc;
  if (date < 1 || date > 31 || month < 1 || month > 12 || year <= 0) {
    document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
  }
  else {
    if ( date == 1) {
      if (month == 5 || month == 7 || month == 10 || month == 12) {
        ngayHomQua = 30;
        thangTruoc = month - 1;
        namTruoc = year;
      }
      else if (month == 2 || month == 4 || month == 6 || month == 8 || month == 9 || month == 11) {
        ngayHomQua = 31;
        thangTruoc = month - 1;
        namTruoc = year;
      }
      else if (month == 1) {
        ngayHomQua = 31;
        thangTruoc = 12;
        namTruoc = year - 1;
      }
      else {
        if (year % 4 == 0 && year % 100 != 0) {
          ngayHomQua = 29;
          thangTruoc = month - 1;
          namTruoc = year;
        }
        else {
          ngayHomQua = 28;
          thangTruoc = month - 1;
          namTruoc = year;
        }
      }
    }
    else {
      ngayHomQua = date - 1;
      thangTruoc = month;
      namTruoc = year;
    }
    document.getElementById("result-b1").innerText = `${ngayHomQua}/${thangTruoc}/${namTruoc}`;
  }
}

function baiTap1b() {
  var date = document.getElementById("date").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var ngayMai, thangSau, namSau;
  if (date < 1 || date > 31 || month < 1 || month > 12 || year <= 0) {
    document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
  }
  else {
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10) {
      if (date == 31) {
        ngayMai = 1;
        thangSau = month + 1;
        namSau = year;
        document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
      }
      else {
        ngayMai = date + 1;
        thangSau = month;
        namSau = year;
        document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
      }
    } 

    else if (month == 4 || month == 6 || month == 9 || month == 11) {
      if (date <= 30) {
        if (date == 30) {
          ngayMai = 1;
          thangSau = month + 1;
          namSau = year;
          document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
        }
        else {
          ngayMai = date + 1;
          thangSau = month;
          namSau = year;
          document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
        }
      }
      else {
        document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
      }
    } 

    else if (month == 2) {
      if (year % 4 == 0 && year % 100 != 0) {
        if (date <= 29) {
          if (date == 29) {
            ngayMai = 1;
            thangSau = month + 1;
            namSau = year;
            document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
          }
          else {
            ngayMai = date + 1;
            thangSau = month;
            namSau = year;
            document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
          }
        }
        else {
          document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
        }
      }
      else {
        if (date <= 28) {
          if (date == 28) {
            ngayMai = 1;
            thangSau = month + 1;
            namSau = year;
            document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
          }
          else {
            ngayMai = date + 1;
            thangSau = month;
            namSau = year;
            document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
          }
        }
        else if (date == 29) {
          document.getElementById("result-b1").innerText = `Năm ${year} không phải là năm nhuận nên tháng 2 không có ngày 29 nhé ^^`;
        }
        else {
          document.getElementById("result-b1").innerText = `Dữ liệu không hợp lệ`;
        }
      }
      
    }
    
    else {
      if (date == 31) {
        ngayMai = 1;
        thangSau = 1;
        namSau = year + 1;
        document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
      }
      else {
        ngayMai = date + 1;
        thangSau = month;
        namSau = year;
        document.getElementById("result-b1").innerText = `${ngayMai}/${thangSau}/${namSau}`;
      }
    }
  }
}
/**
 * Bài tập 2
 * Input: nhập tháng và năm
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến month, year theo giá trị của HTML element
 *    + Khai báo biến ndate
 *    + Dùng cấu trúc if-else:
 *      Nếu month <= 0 || month >= 13 || year <= 0 thì in ra câu "Tháng hoặc năm không hợp lệ"
 *      Nếu không thì:
 *         Nếu month == 2, xét 2 trường hợp năm có nhuần hay không (year % 4 == 0 && year % 100 != 0), nếu năm nhuần thì ndate = 29, ngược lại thì ndate = 28
 *         Nếu month == 1, 3, 5, 7, 9 hoặc 11 thì ndate = 31
 *         Trường hợp còn lại ndate = 30
 *    + In kết quả ra giao diện web
 *
 * Output: số ngày tháng đó (nhập từ input) có
 */

function baiTap2() {
  var month = document.getElementById("month-b2").value * 1;
  var year = document.getElementById("year-b2").value * 1;
  var ndate;
  if (month < 1 || month > 12 || year <= 0) {
    document.getElementById("result-b2").innerText = `Tháng hoặc năm không hợp lệ`;
  }
  else {
    if (month == 2) {
      if (year % 4 == 0 && year % 100 != 0) {
        ndate = 29;
      }
      else {
        ndate = 28;
      }
    }
    else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 9 || month == 11) {
      ndate = 31;
    }
    else {
      ndate = 30;
    }
    document.getElementById("result-b2").innerText = `Tháng ${month} năm ${year} có ${ndate} ngày`;
  }
}

/**
 * Bài tập 3
 * Input: nhập số nguyên n có 3 chữ số
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến n theo giá trị của HTML element
 *    + Khai báo biến hangTram, hangChuc, hangDonVi, chuHangTram, chuHangChuc, chuHangDonVi
 *    + Gán biến hangTram = Math.floor(n / 100)
 *    + Gán biến hangChuc = Math.floor(n % 100 /10)
 *    + Gán biến hangDonVi = Math.floor(n % 10)
 *    + Dùng cấu trúc if-else:
 *      Nếu n không phải là số nguyên có 3 chữ số thì in dòng chữ "Dữ liệu không hợp lệ"
 *      Nếu không thì:
 *         Gán biến chữ chuHangTram theo giá trị số hangTram
 *         Nếu hangChuc = 0 và hangDonVi = 0 thì chuHangChuc và chuHangDonVi có giá trị rỗng
 *         Nếu không thì gán biến chữ chuHangChuc, chuHangDonVi theo giá trị số hangTram, hangDonVi tương ứng
 *    + In kết quả ra giao diện web
 * 
 * Output: cách đọc số nguyên n
 */

function baiTap3() {
  var n = document.getElementById("soNguyen").value * 1;
  var hangTram, hangChuc, hangDonVi, chuHangTram, chuHangChuc, chuHangDonVi;
  hangTram = Math.floor(n / 100);
  hangChuc = Math.floor(n % 100 /10);
  hangDonVi = Math.floor(n % 10);
  
  if (hangTram == 0 || n > 999) {
    document.getElementById("result-b3").innerHTML = `Dữ liệu không hợp lệ`
  }
  else {
    if (hangTram == 1) {
      chuHangTram = "Một trăm"
    }
    else if (hangTram == 2) {
      chuHangTram = "Hai trăm"
    }
    else if (hangTram == 3) {
      chuHangTram = "Ba trăm"
    }
    else if (hangTram == 4) {
      chuHangTram = "Bốn trăm"
    }
    else if (hangTram == 5) {
      chuHangTram = "Năm trăm"
    }
    else if (hangTram == 6) {
      chuHangTram = "Sáu trăm"
    }
    else if (hangTram == 7) {
      chuHangTram = "Bảy trăm"
    }
    else if (hangTram == 8) {
      chuHangTram = "Tám trăm"
    }
    else if (hangTram == 9) {
      chuHangTram = "Chín trăm"
    }

    if (hangChuc == 0 && hangDonVi == 0) {
      chuHangChuc = "";
      chuHangDonVi = "";
    }
    else {
      if (hangChuc == 0) {
        chuHangChuc = "lẻ"
      }
      else if (hangChuc == 1) {
        chuHangChuc = "mười"
      }
      else if (hangChuc == 2) {
        chuHangChuc = "hai mươi"
      }
      else if (hangChuc == 3) {
        chuHangChuc = "ba mươi"
      }
      else if (hangChuc == 4) {
        chuHangChuc = "bốn mươi"
      }
      else if (hangChuc == 5) {
        chuHangChuc = "năm mươi"
      }
      else if (hangChuc == 6) {
        chuHangChuc = "sáu mươi"
      }
      else if (hangChuc == 7) {
        chuHangChuc = "bảy mươi"
      }
      else if (hangChuc == 8) {
        chuHangChuc = "tám mươi"
      }
      else if (hangChuc == 9) {
        chuHangChuc = "chín mươi"
      }

      if (hangDonVi == 0) {
        chuHangDonVi = ""
      }
      else if (hangDonVi == 1) {
        chuHangDonVi = "một"
      }
      else if (hangDonVi == 2) {
        chuHangDonVi = "hai"
      }
      else if (hangDonVi == 3) {
        chuHangDonVi = "ba"
      }
      else if (hangDonVi == 4) {
        chuHangDonVi = "bốn"
      }
      else if (hangDonVi == 5) {
        chuHangDonVi = "năm"
      }
      else if (hangDonVi == 6) {
        chuHangDonVi = "sáu"
      }
      else if (hangDonVi == 7) {
        chuHangDonVi = "bảy"
      }
      else if (hangDonVi == 8) {
        chuHangDonVi = "tám"
      }
      else if (hangDonVi == 9) {
        chuHangDonVi = "chín"
      }
    }
    document.getElementById("result-b3").innerHTML = `${chuHangTram} ${chuHangChuc} ${chuHangDonVi}`
  }
}

/**
 * Bài tập 4
 * Input: nhập tên và tọa độ của tên sinh viên, trường
 *
 * Các bước thực hiện:
 *    + Khai báo và gán biến tenSV1, tenSV2, tenSV3, x1, y1, x2, y2, x3, y3 theo giá trị của HTML element
 *    + Khai báo biến d1, d2, d3, result
 *    + Gán biến d1 = Math.sqrt((x1 - xt) ** 2 + (y1 - yt) ** 2)
 *    + Gán biến d2 = Math.sqrt((x2 - xt) ** 2 + (y2 - yt) ** 2)
 *    + Gán biến d3 = Math.sqrt((x3 - xt) ** 2 + (y3 - yt) ** 2)
 *    + Dùng cấu trúc if-else:
 *      Nếu giá trị tên sinh viên bị rỗng thì in dòng chữ "Dữ liệu không hợp lệ (thiếu tên sinh viên)"
 *      Nếu không thì :
 *          Nếu d1 > d2 && d1 > d3 thì result = tenSV1
 *          Nếu d2 > d1 && d2 > d3 thì result = tenSV2
 *          Trường hợp còn lại result = tenSV3
 *    + In kết quả ra giao diện web
 * 
 * Output: cho biết sinh viên nào xa trường nhất
 */

function baiTap4() {
  var tenSV1 = document.getElementById("ten1").value;
  var tenSV2 = document.getElementById("ten2").value;
  var tenSV3 = document.getElementById("ten3").value;
  var x1 = document.getElementById("x1").value * 1;
  var y1 = document.getElementById("y1").value * 1;
  var x2 = document.getElementById("x2").value * 1;
  var y2 = document.getElementById("y2").value * 1;
  var x3 = document.getElementById("x3").value * 1;
  var y3 = document.getElementById("y3").value * 1;
  var xt = document.getElementById("xt").value * 1;
  var yt = document.getElementById("yt").value * 1;
  var d1, d2, d3, result;

  d1 = Math.sqrt((x1 - xt) ** 2 + (y1 - yt) ** 2);
  d2 = Math.sqrt((x2 - xt) ** 2 + (y2 - yt) ** 2);
  d3 = Math.sqrt((x3 - xt) ** 2 + (y3 - yt) ** 2);

  if (tenSV1 == "" || tenSV2 == "" || tenSV2 == "") {
    document.getElementById("result-b4").innerHTML = `Dữ liệu không hợp lệ (thiếu tên sinh viên)`
  }
  else {
    if (d1 > d2 && d1 > d3) {
      result = tenSV1;
    }
    else if (d2 > d1 && d2 > d3)
      result = tenSV2;
    else {
      result = tenSV3;
    }
    document.getElementById("result-b4").innerHTML = `Sinh viên xa trường nhất: ${result}`;
  }
  
}


